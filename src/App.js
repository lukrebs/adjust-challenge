import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import CustomersPage from "./components/CustomersPage";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={CustomersPage} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
