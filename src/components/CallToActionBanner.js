import React from "react";
import RequestDemoButton from "./common/RequestDemoButton";

const CallToActionBanner = () => {
  return (
    <div className="call-to-action-banner">
      <div className="call-to-action-wrapper">
        <h3>Get better app insights with Adjust</h3>
        <RequestDemoButton />
      </div>
    </div>
  );
};

export default CallToActionBanner;
