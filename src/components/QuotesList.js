import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import QuoteReview from "./QuoteReview";
import ArrowIcon from "./icons/ArrowIcon";

class QuotesList extends Component {
  constructor() {
    super();
    this.state = { slideWidth: 360 };
  }

  componentWillReceiveProps(nextProps) {
    // Always return the quotes to the beginning of the list
    if (!nextProps.quotes) return;

    let videoList = document.getElementById("quote-list");
    videoList.scrollLeft = 0;
  }

  changeSlide = (_e, move) => {
    const { slideWidth } = this.state;
    let videoList = document.getElementById("quote-list");

    videoList.scrollLeft =
      move === "next"
        ? videoList.scrollLeft + slideWidth
        : videoList.scrollLeft - slideWidth;
  };

  renderContent = () => {
    const { quotes } = this.props;
    if (!quotes) return null;

    return _.map(quotes, quoteReview => {
      return (
        <div className="quote-review" key={quoteReview.name}>
          <QuoteReview review={quoteReview} />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="quotes-list bg-white df-padding relative">
        <h3>Mobile marketers recommend Adjust</h3>
        <div id="quote-list" className="db-flex overflow">
          {this.renderContent()}
        </div>
        <ArrowIcon
          className="slide slide-prev"
          onIconClick={e => this.changeSlide(e, "prev")}
        />
        <ArrowIcon
          className="slide slide-next"
          onIconClick={e => this.changeSlide(e, "next")}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ quotes }) => ({ quotes });

export default connect(mapStateToProps)(QuotesList);
