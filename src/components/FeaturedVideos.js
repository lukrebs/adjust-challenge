import React, { Component } from "react";
import { connect } from "react-redux";
import VideoCard from "./VideoCard";
import Quote from "./Quote";
import ArrowIcon from "./icons/ArrowIcon";

class FeaturedVideos extends Component {
  constructor() {
    super();
    this.state = { slideWidth: 340 };
  }

  renderContent = () => {
    const { customers } = this.props;
    if (!customers || !Object.keys(customers).length) return null;

    return Object.keys(customers).map(index => {
      // Do not show the first video, once its already
      // selected in the CurrentVideo component
      if (index === "0") return null;

      // Render the remaining customers videos
      return (
        <div className="video-card-grid" key={customers[index].name}>
          <VideoCard customer={customers[index]} className="light-box-shadow" />
          <div className="quote-video-bottom">
            <Quote quote={customers[index].quote} />
          </div>
        </div>
      );
    });
  };

  changeSlide = (_e, move) => {
    const { slideWidth } = this.state;
    let videoList = document.getElementById("video-card-grid-wrapper");

    videoList.scrollLeft =
      move === "next"
        ? videoList.scrollLeft + slideWidth
        : videoList.scrollLeft - slideWidth;
  };

  render() {
    const { customers } = this.props;
    // Do not show the first video, once its already
    // selected in the CurrentVideo component
    if (!customers || Object.keys(customers).length === 1) return null;

    return (
      <div className="featured-videos df-grid df-padding-b relative">
        <h3>Our customers: in their own words</h3>
        <div id="video-card-grid-wrapper">{this.renderContent()}</div>
        <ArrowIcon
          className="slide slide-prev"
          onIconClick={e => this.changeSlide(e, "prev")}
        />
        <ArrowIcon
          className="slide slide-next"
          onIconClick={e => this.changeSlide(e, "next")}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ customers }) => ({ customers });

export default connect(mapStateToProps)(FeaturedVideos);
