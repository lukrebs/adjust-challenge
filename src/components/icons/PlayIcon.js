import React from "react";

const PlayIcon = () => {
  return (
    <svg viewBox="0 0 12 14" width="100%" height="100%">
      <path d="M0 1v12.02a.99.99 0 0 0 1.49.84l10.03-5.98c.64-.4.64-1.33 0-1.73L1.49.12C.79-.23 0 .22 0 1z" />
    </svg>
  );
};

export default PlayIcon;
