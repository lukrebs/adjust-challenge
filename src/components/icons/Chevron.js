import React from "react";

const Chevron = () => {
  return (
    <svg
      id="chevron"
      viewBox="0 0 9.7 5.8"
      width="100%"
      height="100%"
      style={{ display: "inline" }}
    >
      <path id="Stroke-1" className="st0" d="M252,777.5h23.7" />
      <polyline
        id="Stroke-3"
        className="st0"
        points="257.6,772 252,777.5 257.6,782.9 "
      />
      <path
        d="M9.5,0.3C9.4,0.2,9.2,0.1,9.1,0.1c-0.2,0-0.3,0.1-0.4,0.2l-3.8,4L1,0.3C0.9,0.2,0.8,0.1,0.6,0.1c0,0,0,0,0,0
    c-0.2,0-0.3,0.1-0.4,0.2C0,0.5,0,0.9,0.2,1.1l4.2,4.4c0.1,0.1,0.3,0.2,0.4,0.2c0.2,0,0.3-0.1,0.4-0.2l4.2-4.4
    C9.7,0.9,9.7,0.6,9.5,0.3z"
      />
    </svg>
  );
};

export default Chevron;
