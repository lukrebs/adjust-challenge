import React from "react";

const ArrowIcon = ({ className, onIconClick }) => {
  return (
    <div
      className={className}
      onClick={onIconClick}
      style={{ backgroundImage: "url('/assets/img/icons/arrow-icon.png')" }}
    />
  );
};

export default ArrowIcon;
