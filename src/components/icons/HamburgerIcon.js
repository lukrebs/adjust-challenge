import React from "react";

const HamburgerIcon = ({ shouldAppear, onIconClicked }) => {
  if (!shouldAppear) return null;

  return (
    <div className="hamburguer-icon toolbar-icon" onClick={onIconClicked}>
      <span />
      <span />
      <span />
      <span />
    </div>
  );
};

export default HamburgerIcon;
