import React from "react";

const CloseIcon = ({ shouldAppear, onIconClicked, videoIcon }) => {
  if (!shouldAppear) return null;

  if (videoIcon) {
    return (
      <div className="close-icon video-icon" onClick={onIconClicked}>
        <span />
        <span />
      </div>
    );
  }

  return (
    <div className="close-icon toolbar-icon" onClick={onIconClicked}>
      <span />
      <span />
    </div>
  );
};

export default CloseIcon;
