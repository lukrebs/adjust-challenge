import React, { Component } from "react";
import { connect } from "react-redux";
import { closeCurrentVideo } from "../actions";
import VideoIframe from "./VideoIframe";
import CloseIcon from "./icons/CloseIcon";

class VideoPlayer extends Component {
  closeVideoPlayer = () => {
    document.body.style.overflow = "initial";
    this.props.closeCurrentVideo();
  };

  videoOpen = () => {
    document.body.style.overflow = "hidden";
  };

  render() {
    const { currentVideo } = this.props;
    if (!currentVideo.id) return null;

    this.videoOpen();

    return (
      <div className="video-player-overlay">
        <div className="video-player-wrapper">
          <div className="close-video-wrapper">
            <CloseIcon
              shouldAppear={true}
              videoIcon={true}
              onIconClicked={this.closeVideoPlayer}
            />
          </div>
          <VideoIframe videoID={currentVideo.id} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ currentVideo }) => ({ currentVideo });

export default connect(
  mapStateToProps,
  { closeCurrentVideo }
)(VideoPlayer);
