import React from "react";
import Quote from "./Quote";

const QuoteReview = ({ review }) => {
  if (!review) return null;

  return (
    <div className="quote-item">
      <div
        className="badge-logo"
        style={{ backgroundImage: `url('${review.companyURL}')` }}
      />
      <div className="quote-video-bottom">
        <Quote quote={review.quote} />
      </div>
      <div className="df-flex">
        {review.photoURL && (
          <div
            className="badge-customer"
            style={{ backgroundImage: `url('${review.photoURL}` }}
          />
        )}
        <div className="card-details --with-photo --no-padding">
          <p className="person-name --no-padding">{review.name}</p>
          <p className="person-job --no-padding">{review.job}</p>
        </div>
        <div />
      </div>
    </div>
  );
};

export default QuoteReview;
