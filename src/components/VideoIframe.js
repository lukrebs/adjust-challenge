import React from "react";

const VideoIframe = ({ videoID }) => {
  return (
    <iframe
      title={videoID}
      src={`https://www.youtube.com/embed/${videoID}?rel=0&amp;controls=1&amp;showinfo=0&amp;autoplay=0`}
      datasrc={`https://www.youtube.com/embed/${videoID}?rel=0&amp;controls=1&amp;showinfo=0&amp;autoplay=0`}
      data-cookieconsent="marketing"
      frameBorder="0"
      width="800"
      height="450"
      style={{
        maxWidth: "100%",
        display: "initial"
      }}
      webkitallowfullscreen=""
      mozallowfullscreen=""
      allowFullScreen=""
      tabIndex="-1"
    />
  );
};

export default VideoIframe;
