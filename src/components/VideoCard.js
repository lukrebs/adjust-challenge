import React, { Component } from "react";
import { connect } from "react-redux";
import { setCurrentVideo } from "../actions";
import PlayIcon from "./icons/PlayIcon";

class VideoCard extends Component {
  videoSelected = () => {
    this.props.setCurrentVideo(this.props.customer.videoID);
  };

  render() {
    const { customer, className } = this.props;
    if (!customer) return null;

    return (
      <div className={`video-card ${className}`} onClick={this.videoSelected}>
        <div
          className="video-card-img"
          style={{
            backgroundImage: `url(${customer.photoURL})`
          }}
        >
          <div className="play-icon">
            <PlayIcon />
          </div>
        </div>
        <div className="card-details df-flex">
          <div>
            <p className="person-name">{customer.name}</p>
            <p className="person-job">{customer.job}</p>
          </div>
          <div>
            <img src={customer.companyURL} alt={customer.job} />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { setCurrentVideo }
)(VideoCard);
