import React, { Component } from "react";
import { Link } from "react-router-dom";
import CloseIcon from "../icons/CloseIcon";
import HamburgerIcon from "../icons/HamburgerIcon";
import Logo from "../icons/Logo";
import Navbar from "./Navbar";
import NavbarMobile from "./NavbarMobile";

class Toolbar extends Component {
  constructor() {
    super();
    this.state = { menuOpen: false };
  }

  toggleMenu = () => this.setState({ menuOpen: !this.state.menuOpen });

  render() {
    const { menuOpen } = this.state;

    return (
      <div>
        <div className="toolbar-wrapper">
          <div className="toolbar">
            <div>
              <Link to="/">
                <div className="logo-wrapper">
                  <Logo />
                </div>
              </Link>
            </div>
            <div className="df-flex">
              <Navbar />
              <NavbarMobile shouldAppear={menuOpen} />
              <HamburgerIcon
                onIconClicked={this.toggleMenu}
                shouldAppear={!menuOpen}
              />
              <CloseIcon
                onIconClicked={this.toggleMenu}
                shouldAppear={menuOpen}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Toolbar;
