import React, { Component } from "react";
import Chevron from "../icons/Chevron";
import { navbarLinks } from "../../constants";
import RequestDemoButton from "./RequestDemoButton";

class Navbar extends Component {
  renderContent = () => {
    if (!navbarLinks) return null;

    return navbarLinks.map(link => {
      return (
        <li key={link.text} className="hide-on-tablet">
          <a href="#!">{link.text}</a>
          {link.chevron ? <Chevron /> : null}
        </li>
      );
    });
  };

  render() {
    return (
      <nav>
        <ul>
          {this.renderContent()}
          <li>
            <RequestDemoButton />
          </li>
        </ul>
      </nav>
    );
  }
}

export default Navbar;
