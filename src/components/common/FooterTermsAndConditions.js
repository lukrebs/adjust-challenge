import React from "react";

const FooterTermsAndConditions = () => {
  return (
    <div className="terms-conditions-wrapper">
      <ul>
        <li>
          <a href="#!">Impressum</a>
        </li>
        <li>
          <a href="#!">Privacy Policy</a>
        </li>
        <li>
          <a href="#!">GDPR</a>
        </li>
        <li>
          <a href="#!">Terms</a>
        </li>
        <li>
          <a href="#!">Forget Device</a>
        </li>
      </ul>

      <div>
        <a
          href="https://www.eprivacy.eu/en/privacy-seals/eprivacyseal/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img src="/assets/img/footer/eprivacy.png" alt="E-privacy" />
        </a>
        <a
          href="https://www.privacyshield.gov/welcome"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img
            src="/assets/img/footer/privacy-shield.png"
            alt="Privacy Shield"
          />
        </a>
        <a
          href="https://www.eugdpr.org/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img src="/assets/img/footer/gdpr.png" alt="EUGDPR" />
        </a>
      </div>
    </div>
  );
};

export default FooterTermsAndConditions;
