import React from "react";
import AdjustLogo from "../icons/Logo";

const FooterContent = () => {
  return (
    <div className="footer-row">
      <div className="footer-column">
        <a href="#!">
          <div className="logo-wrapper">
            <AdjustLogo />
          </div>
        </a>
      </div>
      <div className="footer-column">
        <ul>
          <li>Quick Links</li>
          <li>
            <a href="#!">Request a Demo</a>
          </li>
          <li>
            <a href="#!">Customers</a>
          </li>
          <li>
            <a href="#!">Contact Us</a>
          </li>
        </ul>
      </div>
      <div className="footer-column">
        <ul>
          <li>Product</li>
          <li>
            <a href="#!">Attribution</a>
          </li>
          <li>
            <a href="#!">Analytics</a>
          </li>
          <li>
            <a href="#!">Audience Builder</a>
          </li>
          <li>
            <a href="#!">Fraud Prevention</a>
          </li>
          <li>
            <a href="#!">The Adjust Dashboard</a>
          </li>
        </ul>
      </div>
      <div className="footer-column">
        <ul>
          <li>Resources</li>
          <li>
            <a href="#!">Reports</a>
          </li>
          <li>
            <a href="#!">Webinars</a>
          </li>
          <li>
            <a href="#!">Case Studies</a>
          </li>
          <li>
            <a href="#!">Glossary</a>
          </li>
          <li>
            <a href="#!">Blog</a>
          </li>
          <li>
            <a href="#!">Product Updates</a>
          </li>
          <li>
            <a href="#!">Events</a>
          </li>
        </ul>
      </div>
      <div className="footer-column">
        <ul>
          <li>Company</li>
          <li>
            <a href="#!">Media</a>
          </li>
          <li>
            <a href="#!">Team</a>
          </li>
          <li>
            <a href="#!">Careers</a>
          </li>
        </ul>
      </div>
      <div className="footer-column">
        <ul>
          <li>Connect with us</li>
          <li>
            <a href="#!">LinkedIn</a>
          </li>
          <li>
            <a href="#!">Facebook</a>
          </li>
          <li>
            <a href="#!">Twitter</a>
          </li>
          <li>
            <a href="#!">Instagram</a>
          </li>
          <li>
            <a href="#!">YouTube</a>
          </li>
          <li>
            <a href="#!">Slideshare</a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default FooterContent;
