import React, { Component } from "react";
import { navbarLinks } from "../../constants";
import Chevron from "../icons/Chevron";

class NavbarMobile extends Component {
  renderContent = () => {
    return navbarLinks.map(link => {
      return (
        <li key={link.text} className="hide-on-tablet">
          <a href="#!">{link.text}</a>
          {link.chevron ? <Chevron /> : null}
        </li>
      );
    });
  };

  render() {
    if (!this.props.shouldAppear) return null;

    return (
      <div className="navbar-mobile">
        <nav>
          <ul>{this.renderContent()}</ul>
        </nav>
      </div>
    );
  }
}

export default NavbarMobile;
