import React from "react";
import FooterContent from "./FooterContent";
import FooterTermsAndConditions from "./FooterTermsAndConditions";

const Footer = () => {
  return (
    <footer>
      <div className="df-padding-t df-padding-b df-grid df-padding">
        <FooterContent />
      </div>
      <div className="footer-terms-conditions">
        <div className="df-padding-t df-padding-b df-grid df-padding">
          <FooterTermsAndConditions />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
