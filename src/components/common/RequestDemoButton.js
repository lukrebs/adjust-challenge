import React from "react";
import { requestDemoLink } from "../../constants";

const RequestDemoButton = ({ bigButton }) => {
  return (
    <a
      className={`request-demo-button ${bigButton}`}
      href={requestDemoLink}
      target="_blank"
      rel="noopener noreferrer"
    >
      Request a Demo
    </a>
  );
};

export default RequestDemoButton;
