import React from "react";

const CurrentVideo = ({ children }) => {
  return (
    <div className="bg-white">
      <div className="current-video-wrapper df-padding-t df-padding-b df-grid">
        {children}
      </div>
    </div>
  );
};

export default CurrentVideo;
