import React from "react";
import IndustriesBar from "./IndustriesBar";
import IndustryContent from "./IndustryContent";

const CustomersReview = () => {
  return (
    <div>
      <IndustriesBar />
      <IndustryContent />
    </div>
  );
};

export default CustomersReview;
