import React, { Component } from "react";
import { connect } from "react-redux";
import { industriesData } from "../constants";
import {
  setIndustryId,
  fetchIndustryById,
  fetchFeaturedIndustry,
  fetchFeaturedQuotes,
  fetchQuotesByIndustryId
} from "../actions";

class IndustriesBar extends Component {
  renderContent = () => {
    const { currentIndustry } = this.props;
    if (!industriesData || !currentIndustry) return null;

    return industriesData.map(industry => {
      return (
        <div
          key={industry.id}
          onClick={e => this.changeIndustry(e, industry)}
          className={
            industry.id === currentIndustry.id ? "industry-active" : ""
          }
        >
          {industry.name}
        </div>
      );
    });
  };

  changeIndustry = (_e, industry) => {
    const {
      setIndustryId,
      fetchFeaturedIndustry,
      fetchFeaturedQuotes,
      fetchIndustryById,
      fetchQuotesByIndustryId
    } = this.props;

    setIndustryId(industry.id);

    return industry.id === 1
      ? (fetchFeaturedIndustry(), fetchFeaturedQuotes())
      : (fetchIndustryById(industry.id), fetchQuotesByIndustryId(industry.id));
  };

  render() {
    return (
      <div className="db-flex df-padding overflow industries-bar">
        {this.renderContent()}
      </div>
    );
  }
}

const mapStateToProps = ({ currentIndustry, quotes }) => ({
  currentIndustry,
  quotes
});

export default connect(
  mapStateToProps,
  {
    setIndustryId,
    fetchIndustryById,
    fetchFeaturedIndustry,
    fetchFeaturedQuotes,
    fetchQuotesByIndustryId
  }
)(IndustriesBar);
