import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchFeaturedIndustry,
  setIndustryId,
  fetchFeaturedQuotes
} from "../actions";
import Toolbar from "./common/Toolbar";
import Footer from "./common/Footer";
import BannerBrands from "./BannerBrands";
import CustomersReview from "./CustomersReview";
import CallToActionBanner from "./CallToActionBanner";
import VideoPlayer from "./VideoPlayer";

class CustomersPage extends Component {
  componentDidMount() {
    this.props.fetchFeaturedIndustry();
    this.props.fetchFeaturedQuotes();
    this.props.setIndustryId(1);
  }

  render() {
    return (
      <div>
        <Toolbar />
        <BannerBrands />
        <CustomersReview />
        <CallToActionBanner />
        <VideoPlayer />
        <Footer />
      </div>
    );
  }
}

export default connect(
  null,
  { fetchFeaturedIndustry, setIndustryId, fetchFeaturedQuotes }
)(CustomersPage);
