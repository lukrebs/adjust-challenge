import React from "react";

const BannerBrands = () => {
  return (
    <div className="banner-brands-wrapper">
      <div className="banner-brands" />
      <h1 className="df-grid df-padding">
        More than 20,000 apps <br />
        from around the world use Adjust
      </h1>
    </div>
  );
};

export default BannerBrands;
