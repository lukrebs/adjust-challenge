import React from "react";

const Quote = ({ quote, sideQuote }) => {
  return (
    <div className="video-quote-wrapper">
      <div className={`video-quote ${sideQuote ? "side-quote" : "top-quote"}`}>
        <p>{quote}</p>
      </div>
    </div>
  );
};

export default Quote;
