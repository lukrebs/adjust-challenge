import React from "react";
import { connect } from "react-redux";
import CurrentVideo from "./CurrentVideo";
import VideoCard from "./VideoCard";
import Quote from "./Quote";
import FeaturedVideos from "./FeaturedVideos";
import QuotesList from "./QuotesList";

const IndustryContent = ({ customers }) => {
  if (!customers) return null;

  return (
    <div>
      <CurrentVideo>
        <div className="video-card-wrapper df-flex">
          <VideoCard customer={customers[0]} />
        </div>
        <Quote
          sideQuote={true}
          quote={customers[0] ? customers[0].quote : ""}
        />
      </CurrentVideo>
      <FeaturedVideos />
      <QuotesList />
    </div>
  );
};

const mapStateToProps = ({ customers }) => ({ customers });

export default connect(mapStateToProps)(IndustryContent);
