let customersPath = "/assets/img/top-customers";
let postersPath = "/assets/img/video-posters";

export const requestDemoLink =
  "https://www.adjust.com/request-a-demo/see-adjust/";

export const navbarLinks = [
  { text: "Product", chevron: true },
  { text: "Pricing", chevron: false },
  { text: "Learn", chevron: true, url: "#!" },
  { text: "Customers", chevron: false },
  { text: "Partners", chevron: false }
];

export const industriesData = [
  { name: "FEATURED", id: 1 },
  { name: "GAMING", id: 2 },
  { name: "TRAVEL", id: 3 },
  { name: "FINANCE", id: 4 },
  { name: "ECOMMERCE", id: 5 },
  { name: "SUBSCRIPTION", id: 6 }
];

export const featuredVideoData = [
  {
    id: 1,
    name: "Jeff Durian",
    job: "VP Marketing & Ad Monetization, Kongregate",
    videoID: "gVbNIrp9B8c",
    photoURL: `${postersPath}/jeff.png`,
    companyURL: `${customersPath}/kongregate.png`,
    quote:
      "The main benefit we get is showing our ads to users that we know are valuable."
  },
  {
    id: 2,
    name: "Paula Neves",
    job: "Chief Marketing Officer, Gazeus",
    videoID: "72U4CiCAmD4",
    photoURL: `${postersPath}/paula.png`,
    companyURL: `${customersPath}/gazeus.png`,
    quote:
      "We needed to build an entire structure to collect user-level data and Adjust already did that for us."
  },
  {
    id: 3,
    name: "Mark Powlen",
    job: "Head of Growth, Tally",
    videoID: "MWJrH1idHiI",
    photoURL: `${postersPath}/mark.png`,
    companyURL: `${customersPath}/tally.png`,
    quote:
      "The customer support and tech support is far above other partners that I've worked with."
  },
  {
    id: 4,
    name: "Cassandra Chernin",
    videoID: "gPeMUcay9sM",
    job: "Senior Manager Digital Marketing Apps & Display, HomeAdvisor",
    photoURL: `${postersPath}/cassandra.png`,
    companyURL: `${customersPath}/homeadvisor.png`,
    quote:
      "As a marketer, it is really easy to use, and all of our partners are integrated with it."
  },
  {
    id: 5,
    name: "Serge Vartanov",
    videoID: "s3uo97A62Y0",
    job: "Chief Marketing Officer, AutoGravity",
    photoURL: `${postersPath}/serge.png`,
    companyURL: `${customersPath}/auto-gravity.png`,
    quote:
      "As a digital platform, it's important for us to find a partner that can give us accurate visibility into the performance of our marketing channels."
  },
  {
    id: 6,
    name: "Alex Potichnyj",
    videoID: "2wBZiv52GYQ",
    job: "Director of Marketing, Checkout51",
    photoURL: `${postersPath}/alex.png`,
    companyURL: `${customersPath}/checkout51.png`,
    quote:
      "The ability to quickly create cohorts of users and pull them out of Adjust to send them to Facebook for example has been instrumental in re-engaging our users in real tim..."
  },
  {
    id: 7,
    name: "Chris Akhavan",
    videoID: "9zqHhfXMcOg",
    job: "Chief Revenue Officer, Glu",
    photoURL: `${postersPath}/chris.png`,
    companyURL: `${customersPath}/glu.png`,
    quote:
      "We surveyed all the providers on the market and thought that Adjust had the most robust solution."
  }
];

export const featuredQuotes = [
  {
    name: "Brian Han",
    job: "Director of User Acquisition, Hotel Tonight",
    photoURL: `${postersPath}/brian.png`,
    companyURL: `${customersPath}/hotel-tonight.png`,
    quote:
      "With Adjust, we can track and measure whatever we want in real time. It's a fully flexible solution that gives us insight beyond just click and install: we can investigate all our downstream events and optimize towards our goals."
  },
  {
    name: "Kamal Taylor",
    job: "Head of Online & Commerce, Yelp",
    quote:
      "Adjust has given us the tools we need to tie our desktop and mobile data together, giving us a more complete picture of user journeys across platforms. This has allowed us to make more informed marketing decisions and ultimately drive higher return on ad spend.",
    photoURL: `${postersPath}/kamal.png`,
    companyURL: `${customersPath}/yelp.png`
  },
  {
    name: "Oktay Kalem",
    job: "Vice President, Digital Marketing & Sales, Akbank",
    quote:
      "We recognize our business imperative behind connecting with diverse financial backgrounds – people who have different credit scores and financial statuses. Thanks to the Audience Builder, we understand and engage with our customers better than before and deliver them personalized experiences through our ads.",
    photoURL: null,
    companyURL: `${customersPath}/akbank.png`
  },
  {
    name: "John Parides",
    job: "Head of User Acquisition, Glu",
    quote:
      "It can be hard for user acquisition managers to differentiate between install attribution providers given the similar feature sets. Adjust sets itself apart with its excellent account management team. Integrations with new partners are always quick and painless thanks to their knowledgeable support and quick responses.",
    photoURL: `${postersPath}/john.png`,
    companyURL: `${customersPath}/glu.png`
  },
  {
    name: "Moshi Blum",
    job: "User Acquisition Lead, Viber",
    quote:
      "I don’t think any advertiser really understands the amount of fraudulent traffic they are dealing with until they activate the Fraud Prevention Suite. I can’t imagine doing any media buying without it now.",
    photoURL: `${postersPath}/moshi.png`,
    companyURL: `${customersPath}/viber.png`
  }
];

export const industriesCustomers = [
  {
    id: 7,
    industryID: 2,
    industryName: "GAMING",
    name: "Chris Akhavan",
    videoID: "9zqHhfXMcOg",
    job: "Chief Revenue Officer, Glu",
    photoURL: `${postersPath}/chris.png`,
    companyURL: `${customersPath}/glu.png`,
    quote:
      "We surveyed all the providers on the market and thought that Adjust had the most robust solution."
  },
  {
    id: 5,
    industryID: 4,
    industryName: "FINANCE",
    name: "Serge Vartanov",
    videoID: "s3uo97A62Y0",
    job: "Chief Marketing Officer, AutoGravity",
    photoURL: `${postersPath}/serge.png`,
    companyURL: `${customersPath}/auto-gravity.png`,
    quote:
      "As a digital platform, it's important for us to find a partner that can give us accurate visibility into the performance of our marketing channels."
  },
  {
    id: 1,
    name: "Ivan de Quercize",
    videoID: "9a0MdBpdDGw",
    industryName: "TRAVEL",
    industryID: 3,
    job: "Mobile Channels Lead, BlaBlaCar",
    photoURL: "/assets/img/video-posters/ivan.png",
    companyURL: "/assets/img/top-customers/spotfy.png",
    quote:
      "I like working with Adjust for two reasons: the first is the simplicity of the tool, and the second is the accessibility of the account managers."
  },
  {
    id: 6,
    name: "Alex Potichnyj",
    videoID: "2wBZiv52GYQ",
    industryName: "ECOMMERCE",
    industryID: 5,
    job: "Director of Marketing, Checkout51",
    photoURL: `${postersPath}/alex.png`,
    companyURL: `${customersPath}/checkout51.png`,
    quote:
      "The ability to quickly create cohorts of users and pull them out of Adjust to send them to Facebook for example has been instrumental in re-engaging our users in real tim..."
  },
  {
    id: 10,
    name: "Gessica Bicego",
    videoID: "SED1bykilOc",
    industryName: "SUBSCRIPTION",
    industryID: 6,
    job: "Head of Performance, Blinkist",
    photoURL: `${postersPath}/gessica.png`,
    companyURL: `${customersPath}/blinkist.png`,
    quote:
      "Adjust let's us put the entire funnel together. We can see everything - from the beginning of the campaign, the first impression to the end."
  }
];

export const industryQuotes = [
  {
    name: "John Parides",
    job: "Head of User Acquisition, Glu",
    industryName: "GAMING",
    industryID: 2,
    quote:
      "It can be hard for user acquisition managers to differentiate between install attribution providers given the similar feature sets. Adjust sets itself apart with its excellent account management team. Integrations with new partners are always quick and painless thanks to their knowledgeable support and quick responses.",
    photoURL: `${postersPath}/john.png`,
    companyURL: `${customersPath}/glu.png`
  },
  {
    name: "Juha Matikainen",
    job: "General Manager Finland, Seriously",
    industryName: "GAMING",
    industryID: 2,
    quote:
      "The partner setup takes me two seconds - it’s one click in the dashboard, and I’ve got a new segment for a new partner. Since the network integration is handled entirely by the network, it’s completely frictionless.",
    photoURL: `${postersPath}/juha.png`,
    companyURL: `${customersPath}/seriously.png`
  },
  {
    name: "Mr. Aras Şengüz",
    job: "EMEA Mobile & Marketing Director, Netmarble",
    industryName: "GAMING",
    industryID: 2,
    quote:
      "After fully localizing the content, their target was to reach 1 million players after launch in Turkey. Netmarble’s approach included mobile acquisition campaigns, TV advertorials and banners, outdoor advertising, and a combination of viral videos and influencer marketing. They needed to monitor the success of each of their marketing channels in real-time and to measure user retention, ROI, sessions and other KPIs.",
    photoURL: `${postersPath}/aras.png`,
    companyURL: `${customersPath}/seriously.png`
  },
  {
    name: "Andre Kempe",
    job: "Head of Performance Marketing, Lovoo",
    industryName: "GAMING",
    industryID: 2,
    quote:
      "Retargeting is an extremely hot topic, and it’s heavy on the tech and data side. You need the right partners, ideas, and campaigns to get it off the ground. Once you do, the versatility and flexibility makes it one of my favorite tools.",
    photoURL: `${postersPath}/andre.png`,
    companyURL: `${customersPath}/lovoo.png`
  },
  {
    name: "Jeff Durian",
    job: "VP Marketing & Ad Monetization, Kongregate",
    videoID: "gVbNIrp9B8c",
    industryName: "GAMING",
    industryID: 2,
    quote:
      "The main benefit we get is showing our ads to users that we know are valuable.",
    photoURL: `${postersPath}/jeff.png`,
    companyURL: `${customersPath}/kongregate.png`
  },
  {
    name: "Vivian Wang",
    job: "Senior Marketing Manager, Elex",
    industryName: "GAMING",
    industryID: 2,
    quote:
      "We can track interactions in our app in real-time to see how our users engage over their full lifetime. We can define our own events, drill down into each user segment, and export data.",
    photoURL: `${postersPath}/vivian.png`,
    companyURL: `${customersPath}/elex.png`
  },
  {
    name: "Brian Han",
    industryName: "TRAVEL",
    industryID: 3,
    job: "Director of User Acquisition, Hotel Tonight",
    photoURL: `${postersPath}/brian.png`,
    companyURL: `${customersPath}/hotel-tonight.png`,
    quote:
      "With Adjust, we can track and measure whatever we want in real time. It's a fully flexible solution that gives us insight beyond just click and install: we can investigate all our downstream events and optimize towards our goals."
  },
  {
    name: "Christian Eid",
    job: "VP Marketing, Careem",
    industryName: "TRAVEL",
    industryID: 3,
    photoURL: `${postersPath}/christian.png`,
    companyURL: `${customersPath}/careem.png`,
    quote:
      "As we started scaling marketing, we were able to see the impact from different channels. This made our budgeting exercise easier and faster. With Adjust analytics we started shifting budgets across channels and were able to measure meaningful KPIs."
  },
  {
    name: "Mariya Katernyak",
    job: "Mobile Marketing Lead, GoEuro",
    industryName: "TRAVEL",
    industryID: 3,
    photoURL: null,
    companyURL: `${customersPath}/goeuro.png`,
    quote:
      "Adjust has built a very solid bridge between partners in an extremely fragmented mobile marketing ecosystem. Being integrated with all major traffic providers, Adjust gives us an opportunity to setup and start running campaigns in no time. A complete collaboration between advertisers and publishers allows us to optimize towards our targets, and brings more transparency into the market overall."
  },
  {
    name: "Steven Post",
    job: "Director of Marketing, HRS",
    industryName: "TRAVEL",
    industryID: 3,
    photoURL: `${postersPath}/steven.png`,
    companyURL: `${customersPath}/hrs.png`,
    quote:
      "We track all of our apps with Adjust - both B2C and B2B. We have flexibility over what we track, and control over what we sync back to our database. Accuracy and depth of data, plus hands-on support are some of the main drivers of us working together."
  },
  {
    name: "Martin Sieber",
    job: "CO-Founder & VPCO-Founder & VP of Product, GetYourGuide",
    industryName: "TRAVEL",
    industryID: 3,
    photoURL: `${postersPath}/martin.png`,
    companyURL: `${customersPath}/geryourguide.png`,
    quote:
      "We can see the engagement and conversion rate per channel and can make informed decisions on where to spend and where not to. It is important for us to have a reliable and responsive partner that is at the forefront of what is possible. A very flexible tracker generation with deep-linking and more was a key criteria for us and so we decided to go with Adjust."
  },
  {
    name: "Tristan Thomas",
    job: "Head of Marketing, Monzo",
    industryName: "FINANCE",
    industryID: 4,
    photoURL: `${postersPath}/tristan.png`,
    companyURL: `${customersPath}/monzo.png`,
    quote:
      "Facebook is one of the most important communication channels for us, Adjust’s deep integration made our work fast and easy. We were able to unlock and reach new targeting groups and increase our growth tremendously."
  },
  {
    name: "Bartosz Sajewski",
    job: "SEA Manager, N26",
    industryName: "FINANCE",
    industryID: 4,
    photoURL: `${postersPath}/dean.png`,
    companyURL: `${customersPath}/n26.png`,
    quote:
      "For every app marketer interested in tracking in-app events, Adjust is a must."
  },
  {
    name: "Ece Ozturk",
    job: "Director of Digital Performance Marketing, Garanti",
    industryName: "FINANCE",
    industryID: 4,
    photoURL: `${postersPath}/ece.png`,
    companyURL: `${customersPath}/garanti.png`,
    quote:
      "The main motivation to work with Adjust was the ability to track all of our download channels from a single platform. Integration simplicity and accuracy of data were a big advantage."
  },
  {
    name: "Oktay Kalem",
    job: "Vice President, Digital Marketing & Sales, Akbank",
    industryName: "FINANCE",
    industryID: 4,
    photoURL: null,
    companyURL: `${customersPath}/akbank.png`,
    quote:
      "We recognize our business imperative behind connecting with diverse financial backgrounds – people who have different credit scores and financial statuses. Thanks to the Audience Builder, we understand and engage with our customers better than before and deliver them personalized experiences through our ads."
  },
  {
    name: "Kamal Taylor",
    job: "Head of Online & Commerce, Yelp",
    industryName: "ECOMMERCE",
    industryID: 5,
    quote:
      "Adjust has given us the tools we need to tie our desktop and mobile data together, giving us a more complete picture of user journeys across platforms. This has allowed us to make more informed marketing decisions and ultimately drive higher return on ad spend.",
    photoURL: `${postersPath}/kamal.png`,
    companyURL: `${customersPath}/yelp.png`
  },
  {
    name: "Faisal Hammad",
    job: "Global Marketing Manager, Foodpanda",
    industryName: "ECOMMERCE",
    industryID: 5,
    quote:
      "Working with Adjust was a milestone that allowed us to further improve, not only in tracking, but also in efficiency, and visibility. This is not possible without the account management and support we get from their side.",
    photoURL: `${postersPath}/faisal.png`,
    companyURL: `${customersPath}/foodpanda.png`
  },
  {
    name: "Tautvydas Gylys",
    job: "Product Manager, Vinted",
    industryName: "ECOMMERCE",
    industryID: 5,
    quote:
      "We plugged our source data from existing campaigns right into the app, seamlessly carrying on a conversation from the ad campaign down to the first purchase. Within the first initial tests, the conversion to buyer jumped up by 77 %. Let’s say that’s enough to make sure we’ll be building this out for heavy production use.",
    photoURL: `${postersPath}/tautvydas.png`,
    companyURL: `${customersPath}/vinted.png`
  },
  {
    name: "Luis-Daniel Alegria",
    job: "Chief Product Officer, Evino",
    industryName: "ECOMMERCE",
    industryID: 5,
    quote:
      "Thanks to our implementation with Adjust we now have a deep understanding of all the performance channels we work with including all the data points that go along with that. This has helped justify the increase of spend on mobile from our total marketing budget; shifting from just 1% of our total marketing budget to around 35% - which will have a significant impact on our business.",
    photoURL: `${postersPath}/luis.png`,
    companyURL: `${customersPath}/evino.png`
  },
  {
    name: "Moshi Blum",
    job: "User Acquisition Lead, Viber",
    industryName: "SUBSCRIPTION",
    industryID: 6,
    quote:
      "I don’t think any advertiser really understands the amount of fraudulent traffic they are dealing with until they activate the Fraud Prevention Suite. I can’t imagine doing any media buying without it now.",
    photoURL: `${postersPath}/moshi.png`,
    companyURL: `${customersPath}/viber.png`
  },
  {
    name: "Adam Jaffe",
    job: "CMO, ABA English",
    industryName: "SUBSCRIPTION",
    industryID: 6,
    quote:
      "Facebook is one of the most important communication channels for us, and Adjust’s deep integration made our work fast and easy. We were able to unlock and reach new targeting groups and increase our growth tremendously.",
    photoURL: `${postersPath}/adam.png`,
    companyURL: `${customersPath}/aba.png`
  },
  {
    name: "Gunnar Böke",
    job: "Head of Marketing & Business Development, dailyme",
    industryName: "SUBSCRIPTION",
    industryID: 6,
    quote:
      "Installs are nice, but they don’t matter when you’re working with a high-engagement product such as ours. That’s just a foot in the door. To properly evaluate which partners and campaigns are bringing in the right users, we need to know more than just volume: we need to know that they perform.",
    photoURL: `${postersPath}/gunnar.png`,
    companyURL: `${customersPath}/dailyme.png`
  }
];
