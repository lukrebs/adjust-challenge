// Customers segregated by industry
export const FEATURED_CUSTOMERS = "FEATURED_CUSTOMERS";
export const INDUSTRY_CUSTOMERS = "INDUSTRY_CUSTOMERS";

// Features segregated by industry
export const FEATURED_QUOTES = "FEATURED_QUOTES";
export const INDUSTRY_QUOTES = "INDUSTRY_QUOTES";

// State of the current industryID
export const SET_CURRENT_INDUSTRY_ID = "SET_CURRENT_INDUSTRY_ID";

// Set the open iframe video to null
export const CLOSE_CURRENT_VIDEO = "CLOSE_CURRENT_VIDEO";

// Set the current video ID
export const VIDEO_SELECTED = "VIDEO_SELECTED";
