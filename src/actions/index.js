import {
  featuredVideoData,
  industriesCustomers,
  featuredQuotes,
  industryQuotes
} from "../constants";
import {
  FEATURED_CUSTOMERS,
  SET_CURRENT_INDUSTRY_ID,
  INDUSTRY_CUSTOMERS,
  FEATURED_QUOTES,
  INDUSTRY_QUOTES,
  CLOSE_CURRENT_VIDEO,
  VIDEO_SELECTED
} from "./types";

export const fetchFeaturedIndustry = () => {
  return { type: FEATURED_CUSTOMERS, payload: featuredVideoData };
};

export const fetchFeaturedQuotes = () => {
  return { type: FEATURED_QUOTES, payload: featuredQuotes };
};

export const fetchIndustryById = industryID => {
  let customers = industriesCustomers.filter(
    customer => customer.industryID === industryID
  );
  return { type: INDUSTRY_CUSTOMERS, payload: customers };
};

export const fetchQuotesByIndustryId = industryID => {
  let quotes = industryQuotes.filter(quote => quote.industryID === industryID);
  return { type: INDUSTRY_QUOTES, payload: quotes };
};

export const setIndustryId = id => {
  return { type: SET_CURRENT_INDUSTRY_ID, payload: { id } };
};

export const closeCurrentVideo = () => {
  return { type: CLOSE_CURRENT_VIDEO, payload: { id: null } };
};

export const setCurrentVideo = videoID => {
  return { type: VIDEO_SELECTED, payload: { id: videoID } };
};
