import { SET_CURRENT_INDUSTRY_ID } from "../actions/types";

export default function(state = {}, action) {
  switch (action.type) {
    case SET_CURRENT_INDUSTRY_ID:
      return { ...action.payload };

    default:
      return state;
  }
}
