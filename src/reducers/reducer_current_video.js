import { CLOSE_CURRENT_VIDEO, VIDEO_SELECTED } from "../actions/types";

export default function(state = {}, action) {
  switch (action.type) {
    case CLOSE_CURRENT_VIDEO:
      return { ...action.payload };

    case VIDEO_SELECTED:
      return { ...action.payload };

    default:
      return state;
  }
}
