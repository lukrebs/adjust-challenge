import { FEATURED_QUOTES, INDUSTRY_QUOTES } from "../actions/types";

export default function(state = {}, action) {
  switch (action.type) {
    case FEATURED_QUOTES:
      return { ...action.payload };

    case INDUSTRY_QUOTES:
      return { ...action.payload };

    default:
      return state;
  }
}
