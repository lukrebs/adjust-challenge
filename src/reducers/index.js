import { combineReducers } from "redux";
import ReducerIndustryCustomers from "./reducer_industry_customers";
import ReducerQuotes from "./reducer_current_quotes";
import ReducerCurrentVideo from "./reducer_current_video";
import ReducerIndustryID from "./reducer_industry_id";

const rootReducer = combineReducers({
  quotes: ReducerQuotes,
  customers: ReducerIndustryCustomers,
  currentVideo: ReducerCurrentVideo,
  currentIndustry: ReducerIndustryID
});

export default rootReducer;
