import { FEATURED_CUSTOMERS, INDUSTRY_CUSTOMERS } from "../actions/types";

export default function(state = {}, action) {
  switch (action.type) {
    case FEATURED_CUSTOMERS:
      return { ...action.payload };

    case INDUSTRY_CUSTOMERS:
      return { ...action.payload };

    default:
      return state;
  }
}
