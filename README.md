# Adjust | Front-end challenge

### Live version

If you prefer to see this page live, it can be found at: https://adjust-challenge.firebaseapp.com/

### Tech stack:

1. The page was built using React and Redux
2. There is no third party UI library to style the page
3. The style of the page was based on the Adjust homepage style

### Running locally:

In order to run this code locally, follow the steps below:

1. Clone this repo:

```
git clone https://lukrebs@bitbucket.org/lukrebs/adjust-challenge.git adjust-challenge
```

2. Change directory and install the dependencies:

```
cd adjust-challenge
npm install
```

3. Start a local server:

```
npm start
```

Well done! You should be able to access:

```
http://localhost:3000
```

### Running the built version locally:

If you want to run the built version of this app in yout machine, you will have to:

1. Install `serve`:

```
  npm install -g serve
```

2. Generate a build version:

```
  npm run build
```

3. Run the built version:

```
  serve build/index.html
```

4. Access:

```
  http://127.0.0.1:8000/
```
